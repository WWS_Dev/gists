# [NateWeiler](https://nateweiler.github.io/)

![NateWeiler Logo](NW.png)

# This is my personal ![GitHub](GitHub-Logo.png) repository.

---

#### I am a ***Computer Software Engineering Technology*** student with a minor
 in ***Small Business Management***.
 
#### I own & operate [What's Happening Lancaster](https://m.facebook.com/WhatsHappeningLancaster/) that is a local advertising business for my area.

![![What's Happening Lancaster Logo](WHL.png)]

---

#### I recently started [Weiler Web Services](https://github.com/WeilerWebServices). It's the start of a ***Web and Software Developement Team*** I will be building from the ground up.

![Weiler Web Services Logo](WWS.png)

---

# Like to help me with a Donation ![](img/$.png)

[![Donate](Donate-PayPal-green.svg)](https://www.paypal.com/paypalme/NateWeiler)

[![PayPal](PayPal_Donate.png)](https://www.paypal.com/paypalme/NateWeiler)

## Scan QVC to Donate in ![Bitcoin](img/Bitcoin%20Orange%20Gold.png)

![Donate in Bitcoin to](img/Donate-To.png)

## Copy address to Donate in ![Bitcoin](img/Bitcoin%20Orange%20Gold.png)

## ```1N4qcqVb6Smak6SpSP5nH62zRdkEM8JtUu```

---
